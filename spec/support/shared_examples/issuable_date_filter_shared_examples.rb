# frozen_string_literal: true

RSpec.shared_examples 'an issuable date filter', :condition, :resource do
  describe '#condition_value' do
    it 'has the correct value for comparison' do
      expect(subject.condition_value).to eq(3.months.ago.to_date)
    end
  end

  describe '#calculate' do
    it 'calculate false given wrong condition' do
      filter = described_class.new(resource, condition.merge(condition: 'newer_than'))
      expect(filter.calculate).to be(false)
    end
  end
end
