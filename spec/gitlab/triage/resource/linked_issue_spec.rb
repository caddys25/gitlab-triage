# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/resource/linked_issue'

describe Gitlab::Triage::Resource::LinkedIssue do
  include_context 'with network context'

  it_behaves_like 'issuable'

  describe '#link_type' do
    subject { described_class.new(resource) }

    let(:resource) { { link_type: 'relates_to' } }

    it 'returns value from link_type' do
      expect(subject.link_type).to eq('relates_to')
    end
  end
end
