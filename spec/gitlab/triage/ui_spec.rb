# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/ui'

describe Gitlab::Triage::UI do
  describe '.header' do
    it 'returns the text wrapped with two lines' do
      expect(described_class.header('foo bar')).to eq <<~EXPECTED_TEXT
          =======
          foo bar
          =======
      EXPECTED_TEXT
    end

    it 'accepts another char' do
      expect(described_class.header('foo bar', char: '~')).to eq <<~EXPECTED_TEXT
          ~~~~~~~
          foo bar
          ~~~~~~~
      EXPECTED_TEXT
    end
  end

  describe '.debug' do
    it 'returns the text with a debug prefix' do
      expect(described_class.debug('foo bar')).to eq('[DEBUG] foo bar')
    end
  end
end
